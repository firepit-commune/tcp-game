use bevy::prelude::*;

mod plugins;
use plugins::AllPlugins;

fn main() {
    App::new()
        // TODO: Remove in favour of fine-grained imports
        // Even if we end up importing all of the ones included, it'd be better to have plugins we
        // didn't make explicitly listed for easy of review and adjustment.
        .add_plugins(DefaultPlugins)
        .add_plugins(AllPlugins)
        .run();
}

