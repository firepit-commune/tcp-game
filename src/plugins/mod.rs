// This file is the root for all of our Bevy Plugins.

use bevy::app::{PluginGroup, PluginGroupBuilder};

pub struct AllPlugins;

impl PluginGroup for AllPlugins {
    fn build(self) -> PluginGroupBuilder {
        PluginGroupBuilder::start::<Self>()
    }
}

// Interfaces/features that exist only for developers working on features
mod meta_devux;
// Any data that needs be persisted, such as configurations and world saves.
mod meta_persist;
// When there is a need for one or more NPCs to be generated, this will handle that, ensuring that
// batches are fairly balanced with their contexts.
mod npc_gen;
// Essentially, the NPCs' AIs and everything that goes with them.
mod npc_mechs;
// Mechanics that revolve around the player experience. Things that guide, affect, or respond to
// the player's experience.
mod player_mechs;
// Camera, HUD, menus, etc. Everything about giving information to the player or taking input from
// them, but no further.
mod player_ux;
// Generating the world, including NPCs, pre-establshed settlements and events.
mod terra_gen;
// Time, weather, physics, etc.
mod terra_mechs;
// Worldly interaction hints as well as rendering the AV of the world itself.
mod terra_ux;

